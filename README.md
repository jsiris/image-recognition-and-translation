# Image Recognition and Translation
This Python Flask application uses Google Cloud Machine Learning API to recognize images and translate to different languages.

## Installation
** Enable Google Cloud AIPs:**

```
gcloud services enable vision.googleapis.com
gcloud services enable translate.googleapis.com
gcloud services enable storage-component.googleapis.com

```
** Create a bucket in Google Cloud storage.**  
** Clone the repository in your Google Cloud account.**  
** Create python virtual enviroment:** 
```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
deactivate
```
** Modified configuration file:**
```
env_variables:
  CLOUD_STORAGE_BUCKET: _name of your own bucket_
```
** Deploy:** 
```
gloud app deploy
```

## Contact
Shu Jiang: [jshu@pdx.edu]()  
Julie Rutherford: [julie.rutherford@pdx.edu]()  
