from flask import Flask, redirect, render_template, request
from google.cloud import storage
from google.cloud import vision
from google.cloud import translate
import os
import process_text

CLOUD_STORAGE_BUCKET = os.environ.get('CLOUD_STORAGE_BUCKET')

app = Flask(__name__)

@app.route("/")
def homepage():
    return render_template('homepage.html')



@app.route('/upload',methods=['GET','POST'])
def upload():

    # Set photo to upload to bucket
    photo = request.files['file']

    # Create a cloud storage client.
    storage_client = storage.Client()

    # Get the bucket that the file will be uploaded to.
    bucket = storage_client.get_bucket(CLOUD_STORAGE_BUCKET)

    # Create a new blob and upload the file's content.
    blob = bucket.blob(photo.filename)
    blob.upload_from_string(photo.read(), content_type=photo.content_type)

    # Make the blob publicly viewable.
    blob.make_public()

    # Create a Cloud Vision client.
    vision_client = vision.ImageAnnotatorClient()
    
    # Use the Cloud Vision API to label our image.
    source_uri = 'gs://{}/{}'.format(CLOUD_STORAGE_BUCKET, blob.name)
    image = vision.types.Image(source=vision.types.ImageSource(gcs_image_uri=source_uri))
    labels = process_text.label_pic(image)
    
    # Use the Cloud translation API to translate first 5 labels
    result = []
    target = request.form['target_lang']
    length = len(labels)

    # Set the number of labels to be displayed
    j = 0
    if length > 5:
        j = 5
    else:
        j = length
   
    for i in range(j):
        label = labels[i].description
        trans = process_text.translate_text(label, target)
        result.append(trans)
    
    # Redirect to the result page.
    return render_template('result.html', labels=labels, result=result, source_uri=blob.public_url, j=j)

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500

if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)



