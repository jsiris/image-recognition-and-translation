from google.cloud import texttospeech
from google.cloud import translate
from google.cloud import vision

def translate_text(input_str, language):
    """
    Converts text from source language to target langauge.
    """
    
    # Set translation client parameters 
    client = translate.Client()
    result = client.translate(input_str, target_language = language)
    return result

# This function ins not used in this application, but texttospeech could
# be incorporated in a future version of this application
def synthesize_text(input_str, language):
    """
    Synthesize speech from an input string.
    """

    # Set TTS client, input and speaker parameters
    client = texttospeech.TextToSpeechClient()
    syn_input = texttospeech.types.SynthesisInput(text = input_str)
    speaker = texttospeech.types.VoiceSelectionParams(
        language_code=language,
        ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

    # Set audio file configurations
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)

    response = client.synthesize_speech(syn_input, speaker, audio_config)

    with open('transcript.mp3', 'wb') as out:
        out.write(response.audio_content)


def label_pic(image):
    """
    Creates a list of labels for primary objects in an image
    """

    # Set vision client parameters 
    client = vision.ImageAnnotatorClient()
    
    # Process image and return list of labels
    response = client.label_detection(image=image)
    labels = response.label_annotations
    return labels


# Test input below
#if __name__ == "__main__":
    # translate_text("Good morning", "de")
    # synthesize_text("Hello", "en-US")
    # label_pic("https://www.birdlife.org/sites/default/files/styles/1600/public/news/european_turtle_dove_streptopelia_turtur_websitec_revital_salomon.jpg")
